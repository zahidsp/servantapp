import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//imp page
import 'package:servant_app/pages/clients/edit_client_page.dart';

class DetailClientPage extends StatefulWidget {
  final List list;
  final int index;

  DetailClientPage({this.list, this.index});
  @override
  _DetailClientPageState createState() => _DetailClientPageState();
}

class _DetailClientPageState extends State<DetailClientPage> {
  void _delete() {
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        child: ListTile(
          leading: CircleAvatar(
            child: Icon(
              FontAwesomeIcons.trashAlt,
              color: Colors.white,
            ),
            backgroundColor: Colors.redAccent,
          ),
          title: Text(
              "Are you sure want to delete '${widget.list[widget.index]['nama_Client']}' ?"),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {},
          color: Colors.redAccent,
          child: Text('Yes, Delete!',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: Colors.white,
                fontSize: 14,
              )),
        ),
        OutlineButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('No, Cancel',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: Colors.black45,
                fontSize: 14,
              )),
        )
      ],
    );

    showDialog(context: context, child: alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: style.backgroundColor,
        appBar: AppBar(
          title: Text('Detail Klien'),
        ),
        body: Container(
          padding: const EdgeInsets.all(16),
          child: ListView(
            children: <Widget>[
              Container(
                color: Colors.white,
                height: 120,
                width: double.infinity,
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text(widget.list[widget.index]['nama_Client'],
                        style: style.Display1TextStyle.copyWith(
                            color: style.accentColor)),
                    // SizedBox(height: 4),
                    Text(widget.list[widget.index]['alamat'],
                        style: style.Body2TextStyle),
                    // SizedBox(height: 4),
                    Text("${widget.list[widget.index]['no_telepon']}",
                        style: style.Body3TextStyle),
                  ],
                ),
              ),
              SizedBox(
                height: 14,
              ),
              Container(
                height: 120,
                color: Colors.white,
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        width: double.infinity,
                        child: FlatButton.icon(
                          onPressed: () {Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => EditClientPage(
                                    list: widget.list, index: widget.index)));
                      },
                          label: Text(
                            'Edit Klien',
                            style: style.Body1TextStyle.copyWith(
                                color: style.darkBlueIcon),
                          ),
                          icon: Icon(
                            FontAwesomeIcons.pencilAlt,
                            size: 18,
                            color: style.darkBlueIcon,
                          ),
                        ),
                      ),
                    ),
                    Divider(color: style.line),
                    Expanded(
                      child: Container(
                        width: double.infinity,
                        child: FlatButton.icon(
                          onPressed: () {
                            _delete();
                          },
                          label: Text(
                            'Hapus',
                            style: style.Body1TextStyle.copyWith(
                                color: style.redIcon),
                          ),
                          icon: Icon(FontAwesomeIcons.trashAlt,
                              size: 18, color: style.redIcon),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
