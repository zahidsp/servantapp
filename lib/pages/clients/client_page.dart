import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//own import
import 'package:servant_app/components/app_drawer.dart';
import 'package:servant_app/pages/clients/add_client_page.dart';
import 'package:servant_app/pages/clients/detail_client_page.dart';

class ClientPage extends StatefulWidget {
  @override
  _ClientPageState createState() => _ClientPageState();
}

class _ClientPageState extends State<ClientPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Klien'),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(FontAwesomeIcons.search),
            iconSize: 18,
            color: Colors.white,
          )
        ],
      ),
      drawer: Drawer(
        child: AppDrawer(),
      ),
      body: ListView(
        padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
        children: <Widget>[
          ClientList(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => AddClientPage()));
        },
        backgroundColor: style.accentColor,
        child: Icon(
          FontAwesomeIcons.plus,
          color: style.TextColorDark,
          size: 18,
        ),
      ),
    );
  }
}

class ClientList extends StatefulWidget {
  @override
  _ClientListState createState() => _ClientListState();
}

class _ClientListState extends State<ClientList> {
  void _action() {
    showBottomSheet(
        context: context,
        builder: (BuildContext context) => Container(
              padding: const EdgeInsets.all(12),
              height: 160,
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      child: FlatButton.icon(
                        onPressed: () {},
                        label: Text(
                          'Edit Klien',
                          style: style.Body1TextStyle.copyWith(
                              color: style.darkBlueIcon),
                        ),
                        icon: Icon(
                          FontAwesomeIcons.pencilAlt,
                          size: 18,
                          color: style.darkBlueIcon,
                        ),
                      ),
                    ),
                  ),
                  Divider(color: style.line),
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      child: FlatButton.icon(
                        onPressed: () {},
                        label: Text(
                          'Hapus',
                          style: style.Body1TextStyle.copyWith(
                              color: style.redIcon),
                        ),
                        icon: Icon(FontAwesomeIcons.trashAlt,
                            size: 18, color: style.redIcon),
                      ),
                    ),
                  )
                ],
              ),
            ));
  }
  var clientList = [
    {
      'nama_Client': 'Andi Setyawan',
      'alamat': 'Moyudan, Sleman',
      'no_telepon': 0892343782347
    },
    {
      'nama_Client': 'Bima Sena',
      'alamat': 'Tajem, Sleman',
      'no_telepon': 0892345466457
    },
    {
      'nama_Client': 'Cika Putri',
      'alamat': 'Papringan, Sleman',
      'no_telepon': 0892328349324
    },
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: clientList.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
              child: ListTile(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DetailClientPage(
                            list: clientList,
                            index: index,
                          )));
            },
            onLongPress: () {
              _action();
            },
            title: Text(
              clientList[index]['nama_Client'],
              style: style.Display2TextStyle,
            ),
            subtitle: Text(
              clientList[index]['alamat'],
              style: style.Body3TextStyle,
            ),
          ));
        });
  }
}
