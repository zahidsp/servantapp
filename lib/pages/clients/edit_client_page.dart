import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class EditClientPage extends StatefulWidget {
  final List list;
  final int index;

  EditClientPage({this.list, this.index});

  @override
  _EditClientPageState createState() => _EditClientPageState();
}

class _EditClientPageState extends State<EditClientPage> {
  TextEditingController controllerName;
  TextEditingController controllerAddress;
  TextEditingController controllerPhone;

  @override
  void initState() {
    controllerName =
        TextEditingController(text: widget.list[widget.index]['nama_Client']);
    controllerAddress =
        TextEditingController(text: widget.list[widget.index]['alamat']);
    controllerPhone = TextEditingController(
        text: widget.list[widget.index]['no_telepon'].toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Edit Data Klien'),
        ),
        body: Container(
          padding: const EdgeInsets.all(16),
          child: ListView(
            children: <Widget>[
              TextFormField(
                controller: controllerName,
                textCapitalization: TextCapitalization.words,
                decoration: InputDecoration(
                    labelText: 'Nama Klien', labelStyle: style.Body2TextStyle),
              ),
              TextFormField(
                controller: controllerAddress,
                decoration: InputDecoration(
                    labelText: 'Alamat',
                    labelStyle: style.Body2TextStyle),
                keyboardType: TextInputType.emailAddress,
              ),
              TextFormField(
                controller: controllerPhone,
                decoration: InputDecoration(
                    labelText: 'Telepon', labelStyle: style.Body2TextStyle),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: 54),
              Container(
                height: 40,
                width: double.infinity,
                child: FlatButton(
                    onPressed: () {},
                    child: Text('SIMPAN', style: style.ButtonTextStyle),
                    color: style.accentColor),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
