import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;

class AddClientPage extends StatefulWidget {
  @override
  _AddClientPageState createState() => _AddClientPageState();
}

class _AddClientPageState extends State<AddClientPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: style.backgroundColor,
        appBar: AppBar(
          title: Text('Tambah Klien'),
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: ListView(
            children: <Widget>[
              TextFormField(
                textCapitalization: TextCapitalization.words,
                decoration: InputDecoration(
                    labelText: 'Nama Klien', labelStyle: style.Body2TextStyle),
              ),
              TextFormField(
                decoration: InputDecoration(
                    labelText: 'Alamat',
                    labelStyle: style.Body2TextStyle),
                keyboardType: TextInputType.emailAddress,
              ),
              TextFormField(
                decoration: InputDecoration(
                    labelText: 'Telepon', labelStyle: style.Body2TextStyle),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: 54),
              Container(
                height: 40,
                width: double.infinity,
                child: FlatButton(
                    onPressed: () {},
                    child: Text('SIMPAN', style: style.ButtonTextStyle),
                    color: style.accentColor),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
