import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//own import
import 'package:servant_app/components/app_drawer.dart';

class CategoryPage extends StatefulWidget {
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  _addDialog() {
    AlertDialog alertDialog = new AlertDialog(
        backgroundColor: style.backgroundColor,
        contentPadding: const EdgeInsets.all(0),
        title: Text('Tambah Kategori'),
        content: Container(
          height: 160,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                TextFormField(
                  textCapitalization: TextCapitalization.words,
                  decoration: InputDecoration(
                      labelText: 'Nama Kategori',
                      labelStyle: style.Body2TextStyle),
                ),
                SizedBox(height: 24),
                Container(
                  height: 40,
                  width: double.infinity,
                  child: FlatButton(
                      onPressed: () {},
                      child: Text('SIMPAN', style: style.ButtonTextStyle),
                      color: style.accentColor),
                ),
              ],
            ),
          ),
        ));

    showDialog(context: context, child: alertDialog);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Kategori Produk'),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(FontAwesomeIcons.search),
            iconSize: 18,
            color: Colors.white,
          )
        ],
      ),
      drawer: Drawer(
        child: AppDrawer(),
      ),
      body: ListView(
        // padding: const EdgeInsets.only(top: 14),
        children: <Widget>[
          CategoryList(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _addDialog();
        },
        backgroundColor: style.accentColor,
        child: Icon(
          FontAwesomeIcons.plus,
          color: style.TextColorDark,
          size: 18,
        ),
      ),
    );
  }
}

class CategoryList extends StatefulWidget {
  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  int _index;
  _editDialog() {
    AlertDialog alertDialog = new AlertDialog(
        backgroundColor: style.backgroundColor,
        contentPadding: const EdgeInsets.all(0),
        title: Text('Edit Kategori'),
        content: Container(
          height: 160,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                TextFormField(
                  textCapitalization: TextCapitalization.words,
                  decoration: InputDecoration(
                      labelText: 'Nama Kategori',
                      labelStyle: style.Body2TextStyle),
                ),
                SizedBox(height: 24),
                Container(
                  height: 40,
                  width: double.infinity,
                  child: FlatButton(
                      onPressed: () {},
                      child: Text('SIMPAN', style: style.ButtonTextStyle),
                      color: style.accentColor),
                ),
              ],
            ),
          ),
        ));

    showDialog(context: context, child: alertDialog);
  }

  void _action() {
    showBottomSheet(
        context: context,
        builder: (BuildContext context) => Container(
              padding: const EdgeInsets.all(12),
              height: 160,
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      child: FlatButton.icon(
                        onPressed: () {
                          _editDialog();
                        },
                        label: Text(
                          'Edit',
                          style: style.Body1TextStyle.copyWith(
                              color: style.darkBlueIcon),
                        ),
                        icon: Icon(
                          FontAwesomeIcons.pencilAlt,
                          size: 18,
                          color: style.darkBlueIcon,
                        ),
                      ),
                    ),
                  ),
                  Divider(color: style.line),
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      child: FlatButton.icon(
                        onPressed: () {},
                        label: Text(
                          'Hapus',
                          style: style.Body1TextStyle.copyWith(
                              color: style.redIcon),
                        ),
                        icon: Icon(FontAwesomeIcons.trashAlt,
                            size: 18, color: style.redIcon),
                      ),
                    ),
                  )
                ],
              ),
            ));
  }

  var categoryList = [
    {
      'id': '1',
      'name': 'Kaos',
    },
    {
      'id': '2',
      'name': 'Kemeja',
    },
    {
      'id': '3',
      'name': 'Jaket',
    },
    {
      'id': '4',
      'name': 'Blazer',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: categoryList.length,
        itemBuilder: (BuildContext context, int index) {
          _index = index;
          return Container(
              child: ListTile(
            onTap: () {},
            onLongPress: () {
              _action();
            },
            title: Text(
              categoryList[index]['name'],
              style: style.Body1TextStyle,
            ),
          ));
        });
  }
}
