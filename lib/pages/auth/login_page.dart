import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:servant_app/pages/homes/home_page.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:servant_app/services/api.dart';
import 'package:servant_app/styles.dart' as style;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path/path.dart' as path;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

enum LoginStatus { notSignIn, signIn }

class _LoginPageState extends State<LoginPage> {
  LoginStatus _loginStatus = LoginStatus.notSignIn;
  final _key = new GlobalKey<FormState>();
  String username, password;
  bool _secureText = true;

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  _checkForm() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      login();
    }
  }

  login() async {
    try {
      final response = await http
          .post(ApiUrl.login, body: {"nama": username, "password": password});
      final data = jsonDecode(response.body);

      int value = data['value'];
      String msg = data['msg'];
      String id = data['id'];
      String nama = data['name'];
      String email = data['email'];
      String picture = data['picture'];

      if (value == 1) {
        setState(() {
          _loginStatus = LoginStatus.signIn;
          savePref(value, nama, email, picture, id);
          print(nama);
        });
      } else {
        print(data);
      }
    } catch (e) {
      print(e);
    }
  }

  savePref(
      int value, String id, String nama, String email, String picture) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("isLogin", value);
      preferences.setString("id", id);
      preferences.setString("nama", nama);
      preferences.setString("email", email);
      preferences.setString("picture", picture);
      preferences.commit();
    });
  }

  var isLogin;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      isLogin = preferences.getInt("isLogin");

      _loginStatus = isLogin == 1 ? LoginStatus.signIn : LoginStatus.notSignIn;
    });
  }

  signOut() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("isLogin", null);
      preferences.commit();
      _loginStatus = LoginStatus.notSignIn;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    switch (_loginStatus) {
      case LoginStatus.notSignIn:
        return SafeArea(
          child: Scaffold(
            body: ListView(
              padding: const EdgeInsets.all(16),
              children: <Widget>[
                Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                        padding: const EdgeInsets.all(16),
                        child: Image.asset(
                            'assets/images/servant/servantFullblck.png')),
                    SizedBox(height: 24),
                    Text('Masuk untuk melanjutkan.',
                        style: style.HeadTextStyle.copyWith(
                            color: style.TextColorDark, fontSize: 24)),
                    SizedBox(height: 14),
                    Form(
                      key: _key,
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            onSaved: (e) => username = e,
                            textCapitalization: TextCapitalization.words,
                            validator: (e) {
                              if (e.isEmpty) {
                                return 'Username wajib diisi';
                              }
                            },
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                labelText: 'Username',
                                labelStyle: style.Body2TextStyle),
                          ),
                          SizedBox(height: 6),
                          TextFormField(
                            onSaved: (e) => password = e,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              labelText: 'Password',
                              labelStyle: style.Body2TextStyle,
                              suffixIcon: IconButton(
                                onPressed: showHide,
                                icon: Icon(_secureText
                                    ? FontAwesomeIcons.eye
                                    : FontAwesomeIcons.eyeSlash),
                              ),
                            ),
                            obscureText: _secureText,
                            keyboardType: TextInputType.visiblePassword,
                            validator: (e) {
                              if (e.isEmpty) {
                                return 'Password wajib diisi';
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 24),
                    Container(
                      height: 40,
                      width: double.infinity,
                      child: FlatButton(
                          onPressed: () {
                            // Navigator.pushReplacement(context,
                            //     MaterialPageRoute(builder: (context) => HomePage()));
                            _checkForm();
                          },
                          child: Text('LOGIN', style: style.ButtonTextStyle),
                          color: style.accentColor),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
        break;

      case LoginStatus.signIn:
        return HomePage(signOut: signOut);
        break;
    }
  }
}
