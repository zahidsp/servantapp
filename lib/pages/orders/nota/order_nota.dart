import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;

//own import
import 'package:servant_app/pages/orders/nota/order_baru_nota_page.dart';
class NotaOrder extends StatefulWidget {
  @override
  _NotaOrderState createState() => _NotaOrderState();
}

class _NotaOrderState extends State<NotaOrder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nota Order'),
      ),
        body: Stack(
      alignment: AlignmentDirectional.bottomEnd,
      children: <Widget>[
        Center(
            child: Container(
          height: double.infinity,
          width: double.infinity,
          // color: style.greenIcon,
          padding: const EdgeInsets.only(right: 64),
          child: ColorFiltered(
            colorFilter: ColorFilter.mode(
                style.backgroundColor.withOpacity(0.04), BlendMode.dstATop),
            child: Container(
                child: Image.asset('assets/images/servant/bgServant.png', repeat: ImageRepeat.repeatY,)),
          ),
        )),
        OrderBaruNota(),
        Container(
          padding: const EdgeInsets.only(bottom: 18, right: 18),
          height: 180,
          width: 160,
          // color: style.blueIcon,
          child: Column(
            children: <Widget>[
              Expanded(flex: 2,child: Image.asset('assets/images/servant/ttd.png')),
              Expanded(flex: 1,child: Container()),
            ],
          )
        )
      ],
    ));
  }
}
