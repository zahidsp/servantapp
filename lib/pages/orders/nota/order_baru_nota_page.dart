import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;

//own import
import 'package:servant_app/components/nota_order_baru.dart';
import 'package:servant_app/components/garis.dart';

class OrderBaruNota extends StatefulWidget {
  @override
  _OrderBaruNotaState createState() => _OrderBaruNotaState();
}

class _OrderBaruNotaState extends State<OrderBaruNota> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 10, 10, 4),
      padding: const EdgeInsets.all(0),
      // color: Colors.white30,
      // decoration: BoxDecoration(
      //   border: Border.all(color: style.primaryColor),
      // ),
      child: ListView(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            // color: style.primaryColor,
            // decoration: BoxDecoration(
            //     border: Border(bottom: BorderSide(color: style.line))),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                  child: Text('Servant Factory',
                      style: style.Display1TextStyle),
                ),
                SizedBox(height: 2),
                Text('Jalan Wonosari km 11, Bantul',
                    style: style.Body3TextStyle.copyWith(color: style.TextColorDark)),
                Garis(),
                Text('NOTA ORDER',
                    style: style.TitleTextStyle.copyWith(
                        fontWeight: FontWeight.w800, fontSize: 26)),
              ],
            ),
          ),
          // Garis(),
          Container(
            margin: const EdgeInsets.only(bottom: 6),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'No. Order',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    ': 0802JIFFI930NC',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 6),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Nama Order',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    ': 0802JIFFI930NC',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 6),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Tanggal Order',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    ': 0802JIFFI930NC',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 0),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Tanggal Jadi',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    ': 0802JIFFI930NC',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                )
              ],
            ),
          ),
          Garis(),
          Container(
            margin: const EdgeInsets.only(bottom: 6),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Nama Klien',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    ': Rudi Tabuti',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 0),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Telepon',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    ': Rudi Tabuti',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                )
              ],
            ),
          ),
          Garis(),
          // Container(
          //   child: Text('Daftar Order', style: style.Display1TextStyle),
          // ),
          Container(
            child: NotaProduct(),
          ),

          Garis(),
          // Container(
          //   margin: const EdgeInsets.only(bottom: 12),
          //   child: Text('Detail Pembayaran', style: style.Display1TextStyle),
          // ),
          Container(
            margin: const EdgeInsets.only(bottom: 6),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Diskon',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    ': 80000',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 6),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Total',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    ': 80000',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 6),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Uang Muka',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    ': 80000',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 0),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Kekurangan',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    ': 80000',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                )
              ],
            ),
          ),
          Garis(),
          Container(
            margin: const EdgeInsets.only(bottom: 6),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Catatan',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    ': 80000',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                )
              ],
            ),
          ),
          // SizedBox(height: 24),
          Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 18, 0, 18),
              child: Text('Terima kasih telah menjadi mitra kami.',
                  style: style.Body1TextStyle),
            ),
          )
        ],
      ),
    );
  }
}
