import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:servant_app/styles.dart' as style;

class PilihButton extends StatefulWidget {
  final String label;

  PilihButton({this.label});


  @override
  _PilihButtonState createState() => _PilihButtonState();
}

class _PilihButtonState extends State<PilihButton> {
  void _addKlien() {
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        height: 146,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: Text(
                'Pilih Klien',
                style: TextStyle(
                    color: Colors.deepPurple[900],
                    fontWeight: FontWeight.w700,
                    fontSize: 18),
              ),
            ),
            ListTile(
              onTap: () {},
              leading: CircleAvatar(
                child: Icon(
                  LineAwesomeIcons.user_md,
                  color: Colors.white,
                ),
                backgroundColor: style.purpleIcon,
              ),
              title: Text('Klien'),
            ),
           
          ],
        ),
      ),
    );

    showDialog(context: context, child: alertDialog);
  }

  void _addProduk() {
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        height: 146,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: Text(
                'Pilih Produk',
                style: TextStyle(
                    color: Colors.deepPurple[900],
                    fontWeight: FontWeight.w700,
                    fontSize: 18),
              ),
            ),
            ListTile(
              onTap: () {},
              leading: CircleAvatar(
                child: Icon(
                  LineAwesomeIcons.shirtsinbulk,
                  color: Colors.white,
                ),
                backgroundColor: style.purpleIcon,
              ),
              title: Text('Produk'),
            ),
            
          ],
        ),
      ),
    );

    showDialog(context: context, child: alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 32,
      child: FlatButton.icon(
        onPressed: () {
          if(widget.label == 'Klien'){_addKlien();}
          else if(widget.label == 'Produk'){_addProduk();}
          
        },
        icon: Icon(LineAwesomeIcons.plus_circle, color: Colors.white, size: 18),
        color: style.greenIcon,
        label: Text(
          widget.label,
          style:
              style.Body1TextStyle.copyWith(color: Colors.white, fontSize: 12),
        ),
      ),
    );
  }
}
