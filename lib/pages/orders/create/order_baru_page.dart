import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:servant_app/styles.dart' as style;
// import 'dart:async';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

//own import
import 'package:servant_app/components/cart_product.dart';
import 'package:servant_app/pages/orders/create/order_buttons.dart';
import 'package:servant_app/pages/orders/nota/order_nota.dart';

class OrderBaruPage extends StatefulWidget {
  @override
  _OrderBaruPageState createState() => _OrderBaruPageState();
}

class _OrderBaruPageState extends State<OrderBaruPage> {
  String _name;
  DateTime _tanggalOrder = DateTime.now();
  DateTime _tanggalJadi = DateTime.now();
  File _image;

  Future _imageGalerry() async {
    var _imageFile;

    try {
      _imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    } catch (e) {
      print(e);
    }

    setState(() {
      _image = _imageFile;
    });
  }

  Future _imageCamera() async {
    var _imageFile;
    try {
      _imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    } catch (e) {
      print(e);
    }
    setState(() {
      _image = _imageFile;
    });
  }

  void _confirm() {
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        height: 146,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: Text(
                'Choose Action',
                style: TextStyle(
                    color: Colors.deepPurple[900],
                    fontWeight: FontWeight.w700,
                    fontSize: 18),
              ),
            ),
            ListTile(
              onTap: () {
                _imageCamera();
                Navigator.pop(context);
              },
              leading: CircleAvatar(
                child: Icon(
                  LineAwesomeIcons.camera,
                  color: Colors.white,
                ),
                backgroundColor: style.purpleIcon,
              ),
              title: Text('Camera'),
            ),
            ListTile(
              onTap: () {
                _imageGalerry();
                Navigator.pop(context);
              },
              leading: CircleAvatar(
                child: Icon(
                  LineAwesomeIcons.folder,
                  color: Colors.white,
                ),
                backgroundColor: style.orangeIcon,
              ),
              title: Text('Gallery'),
            )
          ],
        ),
      ),
    );

    showDialog(context: context, child: alertDialog);
  }

  String _validateName(String value) {
    if (value.isEmpty) return 'Nama wajib diisi.';
    final RegExp nameExp = new RegExp(r'^[A-Za-z ]+$');
    if (!nameExp.hasMatch(value)) return 'Iputkan huruf saja.';
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order Baru'),
      ),
      body: ListView(
        padding: const EdgeInsets.all(16.0),
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Text(
                  'Data Klien',
                  style: style.Display1TextStyle,
                ),
              ),
              Expanded(flex: 1, child: PilihButton(label: 'Klien'))
            ],
          ),
          TextFormField(
            textCapitalization: TextCapitalization.words,
            decoration: const InputDecoration(
                labelText: 'Nama Klien *', labelStyle: style.Body2TextStyle),
            maxLines: 1,
            onSaved: (String value) {
              this._name = value;
            },
            validator: _validateName,
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: TextFormField(
                  decoration: const InputDecoration(
                      labelText: 'Alamat *', labelStyle: style.Body2TextStyle),
                  maxLines: 1,
                  onSaved: (String value) {
                    this._name = value;
                  },
                  validator: _validateName,
                ),
              ),
              SizedBox(
                width: 12,
              ),
              Expanded(
                child: TextFormField(
                  decoration: const InputDecoration(
                      labelText: 'No. Telepon *',
                      labelStyle: style.Body2TextStyle),
                  maxLines: 1,
                  onSaved: (String value) {
                    this._name = value;
                  },
                  keyboardType: TextInputType.phone,
                  validator: _validateName,
                ),
              ),
            ],
          ),
          SizedBox(height: 18),
          Padding(
              padding: const EdgeInsets.only(top: 14.0),
              child: Text(
                'Data Order',
                style: style.Display1TextStyle,
              )),
          Row(
            children: <Widget>[
              Expanded(
                child: TextFormField(
                  decoration: const InputDecoration(
                      labelText: 'No. Order', labelStyle: style.Body2TextStyle),
                  maxLines: 1,
                  onSaved: (String value) {
                    this._name = value;
                  },
                  validator: _validateName,
                ),
              ),
              SizedBox(
                width: 12,
              ),
              Expanded(
                child: TextFormField(
                  textCapitalization: TextCapitalization.words,
                  decoration: const InputDecoration(
                      labelText: 'Nama Order *',
                      labelStyle: style.Body2TextStyle),
                  maxLines: 1,
                  onSaved: (String value) {
                    this._name = value;
                  },
                  keyboardType: TextInputType.phone,
                  validator: _validateName,
                ),
              ),
            ],
          ),
          SizedBox(height: 4),
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: ListTile(
                    title: Text('Tanggal Order', style: style.Body2TextStyle),
                    subtitle: Text('${_tanggalOrder.toLocal()}',
                        style: style.Body3TextStyle),
                    onTap: () {
                      DatePicker.showDatePicker(context,
                          showTitleActions: true,
                          minTime: DateTime(2017, 1, 1),
                          maxTime: DateTime(2021, 12, 31), onConfirm: (date) {
                        setState(() {
                          _tanggalOrder = date;
                        });
                      });
                    },
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: ListTile(
                    // isThreeLine: true,
                    title: Text('Tanggal Jadi', style: style.Body2TextStyle),
                    subtitle: Text('${_tanggalJadi.toLocal()}',
                        style: style.Body3TextStyle),
                    onTap: () {
                      DatePicker.showDatePicker(
                        context,
                        showTitleActions: true,
                        minTime: DateTime(2017, 1, 1),
                        maxTime: DateTime(2021, 12, 31),
                        onConfirm: (date) {
                          setState(() {
                            _tanggalJadi = date;
                          });
                        },
                        // currentTime: DateTime.now(),
                      );
                    },
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 18),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Text(
                  'Data Produk',
                  style: style.Display1TextStyle,
                ),
              ),
              Expanded(
                flex: 1,
                child:
                    Container(height: 32, child: PilihButton(label: 'Produk')),
              )
            ],
          ),
          Container(
            child: CartProduct(),
          ),
          SizedBox(height: 18),
          Padding(
              padding: const EdgeInsets.only(top: 14.0),
              child: Text(
                'Pembayaran',
                style: style.Display1TextStyle,
              )),
          Row(
            children: <Widget>[
              Expanded(
                child: TextFormField(
                  decoration: const InputDecoration(
                      labelText: 'Diskon',
                      labelStyle: style.Body2TextStyle,
                      prefixText: 'Rp ',
                      prefixStyle: TextStyle(color: style.greenIcon),
                      suffixText: 'IDR',
                      suffixStyle: TextStyle(color: style.greenIcon)),
                  maxLines: 1,
                  onSaved: (String value) {
                    this._name = value;
                  },
                  keyboardType: TextInputType.phone,
                  validator: _validateName,
                ),
              ),
              SizedBox(width: 12),
              Expanded(
                child: TextFormField(
                  decoration: const InputDecoration(
                      labelText: 'Total',
                      labelStyle: style.Body2TextStyle,
                      prefixText: 'Rp ',
                      prefixStyle: TextStyle(color: style.greenIcon),
                      suffixText: 'IDR',
                      suffixStyle: TextStyle(color: style.greenIcon)),
                  maxLines: 1,
                  onSaved: (String value) {
                    this._name = value;
                  },
                  keyboardType: TextInputType.phone,
                  validator: _validateName,
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: TextFormField(
                  decoration: const InputDecoration(
                      labelText: 'Uang Muka',
                      labelStyle: style.Body2TextStyle,
                      prefixText: 'Rp ',
                      prefixStyle: TextStyle(color: style.greenIcon),
                      suffixText: 'IDR',
                      suffixStyle: TextStyle(color: style.greenIcon)),
                  maxLines: 1,
                  onSaved: (String value) {
                    this._name = value;
                  },
                  keyboardType: TextInputType.phone,
                  validator: _validateName,
                ),
              ),
              SizedBox(width: 12),
              Expanded(
                child: TextFormField(
                  decoration: const InputDecoration(
                      // border: OutlineInputBorder(),
                      labelText: 'Kekurangan',
                      labelStyle: style.Body2TextStyle,
                      prefixText: 'Rp ',
                      prefixStyle: TextStyle(color: style.greenIcon),
                      suffixText: 'IDR',
                      suffixStyle: TextStyle(color: style.greenIcon)),
                  maxLines: 1,
                  onSaved: (String value) {
                    this._name = value;
                  },
                  keyboardType: TextInputType.phone,
                  validator: _validateName,
                ),
              ),
            ],
          ),
          SizedBox(height: 18),
          Padding(
              padding: const EdgeInsets.only(top: 14.0),
              child: Text(
                'Lainnya',
                style: style.Display1TextStyle,
              )),
          SizedBox(height: 12),
          Text('Gambar', style: style.Body2TextStyle),
          SizedBox(height: 8),
          Container(
            // margin: const EdgeInsets.only(top: 22),
            // height: 120.0,
            width: double.infinity,
            // color: Colors.black12,
            child: Center(
                child: _image == null
                    ? InkWell(
                        onTap: () {
                          _confirm();
                        },
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                                  FontAwesomeIcons.plusSquare,
                                  size: 48.0,
                                  color: style.TextColor1,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 10,bottom: 18),
                                  child: Text('Add new image',
                                      style: style.Body2TextStyle.copyWith(color: style.TextColor1)),
                                ),
                          ],
                        ),
                      )
                    : Center(
                        child: Stack(
                          alignment: AlignmentDirectional.topEnd,
                          children: <Widget>[
                            Image.file(_image),
                            Container(
                              height: 40,
                              width: 110,
                              color: Colors.black12.withOpacity(0.3),
                              child: FlatButton.icon(
                                onPressed: () {
                                  _confirm();
                                },
                                icon: Icon(
                                  LineAwesomeIcons.pencil,
                                  color: Colors.white,
                                  size: 10,
                                ),
                                label: Text(
                                  'Edit Image',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 10),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )),
          ),
          TextFormField(
            decoration: const InputDecoration(
              labelText: 'Catatan',
              labelStyle: style.Body2TextStyle,
            ),
            maxLines: 3,
            onSaved: (String value) {
              this._name = value;
            },
          ),
          SizedBox(height: 18),

          Container(
            height: 40,
            width: double.infinity,
            child: FlatButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => NotaOrder()));
                },
                child: Text('SIMPAN', style: style.ButtonTextStyle),
                color: style.accentColor),
          ),
        ],
      ),
    );
  }
}
