import 'package:flutter/material.dart';

//own import


class OrderProdukPage extends StatefulWidget {
  @override
  _OrderProdukPageState createState() => _OrderProdukPageState();
}

class _OrderProdukPageState extends State<OrderProdukPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Pilih Produk'),
        ),
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            Container(
              child: ProdukList(),
            )
          ],
        ),
      ),
    );
  }
}

class ProdukList extends StatefulWidget {
  @override
  _ProdukListState createState() => _ProdukListState();
}

class _ProdukListState extends State<ProdukList> {
  var productList = [
    {
      'kategori': 'Blazer',
      'nama': 'Blazer Two',
      'picture': 'images/products/blazer1.jpeg',
      'harga': 1200000,
      'ukuran': 'M',
      'catatan': 'Black',
      'qty': 2,
      'counter': 7,
    },
    {
      'kategori': 'Dress',
      'nama': 'Dress One',
      'picture': 'images/products/dress1.jpeg',
      'harga': 700000,
      'ukuran': 'M',
      'catatan': 'Red',
      'qty': 1,
      'counter': 10,
    },
    {
      'kategori': 'Celana',
      'nama': 'Celana Two',
      'picture': 'images/products/pants1.jpg',
      'harga': 1200000,
      'ukuran': 'M',
      'catatan': 'Black',
      'qty': 2,
      'counter': 7,
    },
    {
      'kategori': 'Kaos',
      'nama': 'Kaos One',
      'picture': 'images/products/skt2.jpeg',
      'harga': 700000,
      'ukuran': 'M',
      'catatan': 'Red',
      'qty': 1,
      'counter': 10,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: productList.length,
      itemBuilder: (BuildContext context, int index) {
        return SingleProduct(
          prodKategori: productList[index]['kategori'],
          prodNama: productList[index]['nama'],
          prodPict: productList[index]['picture'],
          prodHarga: productList[index]['harga'],
          prodUkuran: productList[index]['ukuran'],
          prodCatatan: productList[index]['catatan'],
          prodQty: productList[index]['qty'],
          prodCounter: productList[index]['counter'],
        );
      },
    );
  }
}

class SingleProduct extends StatelessWidget {
  final prodKategori;
  final prodNama;
  final prodPict;
  final prodHarga;
  final prodUkuran;
  final prodCatatan;
  final prodQty;
  final prodCounter;

  SingleProduct(
      {this.prodKategori,
      this.prodNama,
      this.prodPict,
      this.prodHarga,
      this.prodUkuran,
      this.prodCatatan,
      this.prodQty,
      this.prodCounter});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _settingModalBottomSheet(context);
      },
      child: Container(
        height: 80,
        margin: const EdgeInsets.only(bottom: 12),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Container(
                  margin: const EdgeInsets.only(right: 16),
                  child: Image.asset(
                    prodPict,
                    fit: BoxFit.cover,
                  ),
                )),
            Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 4),
                    child: Text(
                      prodNama,
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 4),
                    child: Text(
                      prodKategori,
                      style: TextStyle(
                          color: Colors.black54,
                          // fontSize: 18,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Text('$prodCounter' + ' Terjual'),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

void _settingModalBottomSheet(context) {
  showModalBottomSheet(
    isScrollControlled: true,
    context: context,
    builder: (BuildContext bc) {
      return Container(
        height: 540.0,
        child: Column(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.only(right: 12),
              color: Colors.blue,
              height: 48.0,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.close),
                    color: Colors.white,
                  ),
                  Text(
                    'Reglan 3/4',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 16),
                  ),
                  FlatButton(
                    onPressed: () {},
                    child: Text(
                      'Simpan',
                      style: TextStyle(color: Colors.white),
                    ),
                    // color: Colors.white,
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
              child: new Wrap(
                children: <Widget>[
                  TextFormField(
                    decoration: const InputDecoration(
                        // border: OutlineInputBorder(),
                        labelText: 'Harga',
                        prefixText: 'Rp ',
                        prefixStyle: TextStyle(color: Colors.green),
                        suffixText: 'IDR',
                        suffixStyle: TextStyle(color: Colors.green)),
                    maxLines: 1,
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                        // border: OutlineInputBorder(),
                        labelText: 'Quantity',
                        suffixText: 'pcs',
                        suffixStyle: TextStyle(color: Colors.green)),
                    maxLines: 1,
                  ),
                  TextFormField(
                    textCapitalization: TextCapitalization.words,
                    decoration: const InputDecoration(
                      // border: OutlineInputBorder(),
                      labelText: 'Ukuran',
                    ),
                    maxLines: 1,
                  ),
                  TextFormField(
                    // textCapitalization: TextCapitalization.words,
                    decoration: const InputDecoration(
                      // border: OutlineInputBorder(),
                      labelText: 'Catatan',
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );
    },
  );
}
