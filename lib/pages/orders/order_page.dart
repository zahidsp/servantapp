import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//own import
import 'package:servant_app/components/app_drawer.dart';

class OrderPage extends StatefulWidget {
  @override
  _OrderPageState createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order'),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(FontAwesomeIcons.search),
            iconSize: 18,
            color: Colors.white,
          )
        ],
      ),
      drawer: Drawer(
        child: AppDrawer(),
      ),
      body: ListView(
        // padding: const EdgeInsets.only(top: 10),
        children: <Widget>[
          OrderList(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: style.accentColor,
        child: Icon(
          FontAwesomeIcons.plus,
          color: style.TextColorDark,
          size: 18,
        ),
      ),
    );
  }
}

class OrderList extends StatefulWidget {
  @override
  _OrderListState createState() => _OrderListState();
}

class _OrderListState extends State<OrderList> {
  void _action() {
    showBottomSheet(
        context: context,
        builder: (BuildContext context) => Container(
              padding: const EdgeInsets.all(12),
              height: 160,
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      child: FlatButton.icon(
                        onPressed: () {},
                        label: Text(
                          'Edit',
                          style: style.Body1TextStyle.copyWith(
                              color: style.darkBlueIcon),
                        ),
                        icon: Icon(
                          FontAwesomeIcons.pencilAlt,
                          size: 18,
                          color: style.darkBlueIcon,
                        ),
                      ),
                    ),
                  ),
                  Divider(color: style.line),
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      child: FlatButton.icon(
                        onPressed: () {},
                        label: Text(
                          'Hapus',
                          style: style.Body1TextStyle.copyWith(
                              color: style.redIcon),
                        ),
                        icon: Icon(FontAwesomeIcons.trashAlt,
                            size: 18, color: style.redIcon),
                      ),
                    ),
                  )
                ],
              ),
            ));
  }

  var orderList = [
    {
      'nama': 'Kaos Jos',
      'picture': 'assets/images/products/w3.jpeg',
      'tanggal_order': '23-11-2019',
      'tanggal_jadi': '25-12-2019',
      'total': 3600000,
      'lunas': false,
      'produksi': 1,
    },
    {
      'nama': 'Blazer Oke',
      'picture': 'assets/images/products/blazer1.jpeg',
      'tanggal_order': '23-11-2019',
      'tanggal_jadi': '25-12-2019',
      'total': 3600000,
      'lunas': false,
      'produksi': 2,
    },
    {
      'nama': 'Jaket Heha',
      'picture': 'assets/images/products/w4.jpeg',
      'tanggal_order': '23-09-2019',
      'tanggal_jadi': '25-10-2019',
      'total': 3600000,
      'lunas': true,
      'produksi': 3,
    },
  ];
  Widget _lunas(lunas) {
    if (lunas) {
      return Text(
        'Lunas',
        style: style.Body2TextStyle.copyWith(fontSize: 12),
      );
    } else {
      return Text(
        'Belum Lunas',
        style: style.Body2TextStyle.copyWith(fontSize: 12),
      );
    }
  }

  Widget _produksi(produksi) {
    if (produksi == 1) {
      return Container(
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
          border: Border.all(color: style.orangeIcon),
          borderRadius: BorderRadius.all(Radius.circular(18.0)),
        ),
        child: Text('Belum Produksi',
            style: style.Body2TextStyle.copyWith(
                color: style.orangeIcon, fontSize: 12)),
      );
    } else if (produksi == 2) {
      return Container(
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
          border: Border.all(color: style.darkBlueIcon),
          borderRadius: BorderRadius.all(Radius.circular(18.0)),
        ),
        child: Text('Sudah Produksi',
            style: style.Body2TextStyle.copyWith(
                color: style.darkBlueIcon, fontSize: 12)),
      );
    } else {
      return Container(
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
          border: Border.all(color: style.greenIcon),
          borderRadius: BorderRadius.all(Radius.circular(18.0)),
        ),
        child: Text('Selesai',
            style: style.Body2TextStyle.copyWith(
                color: style.greenIcon, fontSize: 12)),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: orderList.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
              margin: const EdgeInsets.symmetric(vertical: 6),
              child: ListTile(
                onTap: () {},
                onLongPress: () {
                  _action();
                },
                leading: Container(
                  width: 42,
                  height: 42,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Image.asset(
                      orderList[index]['picture'],
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                title: Container(
                  child: Row(
                    children: <Widget>[
                      Text(
                        orderList[index]['tanggal_order'],
                        style: style.Body3TextStyle.copyWith(fontSize: 12),
                      ),
                      Text(
                        ' | ',
                        style: style.Body3TextStyle.copyWith(fontSize: 12),
                      ),
                      Text(
                        orderList[index]['tanggal_jadi'],
                        style: style.Body3TextStyle.copyWith(fontSize: 12),
                      ),
                    ],
                  ),
                ),
                subtitle: Container(
                  margin: const EdgeInsets.only(top: 4),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text(
                        orderList[index]['nama'],
                        style: style.Display2TextStyle,
                      ),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Text(
                              '${orderList[index]['total']}',
                              style: style.Body2TextStyle,
                            ),
                            Text(
                              ' - ',
                              style: style.Body2TextStyle,
                            ),
                            _lunas(orderList[index]['lunas']),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                trailing: _produksi(orderList[index]['produksi']),
              ));
        });
  }
}
