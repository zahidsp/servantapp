import 'package:flutter/material.dart';

//own import
import '../orders/create/order_baru_page.dart';

class CustomerChoosePage extends StatefulWidget {
  @override
  _CustomerChoosePageState createState() => _CustomerChoosePageState();
}

class _CustomerChoosePageState extends State<CustomerChoosePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Pilih Customer'),
        ),
        body: ListView(
          padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
          children: <Widget>[
            CustomerList(),
          ],
        ),
      ),
    );
  }
}

class CustomerList extends StatefulWidget {
  @override
  _CustomerListState createState() => _CustomerListState();
}

class _CustomerListState extends State<CustomerList> {
  var customerList = [
    {
      'nama_customer': 'Andi Setyawan',
      'alamat': 'Moyudan, Sleman',
      'no_telepon': 0892343782347
    },
    {
      'nama_customer': 'Bima Sena',
      'alamat': 'Tajem, Sleman',
      'no_telepon': 0892345466457
    },
    {
      'nama_customer': 'Cika Putri',
      'alamat': 'Papringan, Sleman',
      'no_telepon': 0892328349324
    },
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: customerList.length,
      itemBuilder: (BuildContext context, int index){
        return SingleCustomer(
          custName: customerList[index]['nama_customer'],
          custAddress: customerList[index]['alamat'],
          custPhone: customerList[index]['no_telepon'],
        );
      }
    );
  }
}

class SingleCustomer extends StatelessWidget {
  final custName;
  final custAddress;
  final custPhone;

  SingleCustomer({this.custName, this.custAddress, this.custPhone});

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListTile(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => OrderBaruPage()));
          },
      title: Text(
        '$custName',
        style: TextStyle(color: Colors.black54),
      ),
      subtitle: Text(
        '$custAddress',
        style: TextStyle(color: Colors.black54),
      ),
    ));
  }
}
