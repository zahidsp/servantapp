import 'package:flutter/material.dart';
import 'package:async/async.dart';
import 'dart:io';
import 'dart:math';
import 'package:image_picker/image_picker.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:servant_app/styles.dart' as style;

class EditPage extends StatefulWidget {
  @override
  _EditPageState createState() => _EditPageState();
}

class _EditPageState extends State<EditPage> {
  File _image;

  Future _imageGalerry() async {
    var _imageFile;

    try {
      _imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    } catch (e) {
      print(e);
    }

    setState(() {
      _image = _imageFile;
    });
  }

  Future _imageCamera() async {
    var _imageFile;
    try {
      _imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    } catch (e) {
      print(e);
    }
    setState(() {
      _image = _imageFile;
    });
  }

  void _confirm() {
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        height: 146,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: Text(
                'Choose Action',
                style: TextStyle(
                    color: Colors.deepPurple[900],
                    fontWeight: FontWeight.w700,
                    fontSize: 18),
              ),
            ),
            ListTile(
              onTap: () {
                _imageCamera();
                Navigator.pop(context);
              },
              leading: CircleAvatar(
                child: Icon(
                  LineAwesomeIcons.camera,
                  color: Colors.white,
                ),
                backgroundColor: style.purpleIcon,
              ),
              title: Text('Camera'),
            ),
            ListTile(
              onTap: () {
                _imageGalerry();
                Navigator.pop(context);
              },
              leading: CircleAvatar(
                child: Icon(
                  LineAwesomeIcons.folder,
                  color: Colors.white,
                ),
                backgroundColor: style.orangeIcon,
              ),
              title: Text('Gallery'),
            )
          ],
        ),
      ),
    );

    showDialog(context: context, child: alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: style.backgroundColor,
        appBar: AppBar(
          title: Text('Edit Profil'),
        ),
        body: Container(
          padding: const EdgeInsets.all(16),
          child: ListView(
            children: <Widget>[
              Container(
                width: double.infinity,
                child: Center(
                    child: _image == null
                        ? InkWell(
                            onTap: () {
                              _confirm();
                            },
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  LineAwesomeIcons.plus,
                                  size: 48.0,
                                  color: Colors.black45,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 18),
                                  child: Text(
                                    'Add new image',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      color: Colors.black45,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        : Center(
                            child: Stack(
                              alignment: AlignmentDirectional.topEnd,
                              children: <Widget>[
                                Image.file(_image),
                                Container(
                                  height: 40,
                                  width: 110,
                                  color: Colors.black12.withOpacity(0.3),
                                  child: FlatButton.icon(
                                    onPressed: () {
                                      _confirm();
                                    },
                                    icon: Icon(
                                      LineAwesomeIcons.pencil,
                                      color: Colors.white,
                                      size: 10,
                                    ),
                                    label: Text(
                                      'Edit Image',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 10),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
              ),
              TextFormField(
                textCapitalization: TextCapitalization.words,
                decoration: InputDecoration(
                    labelText: 'Name', labelStyle: style.Body2TextStyle),
              ),
              TextFormField(
                decoration: InputDecoration(
                    labelText: 'Email', labelStyle: style.Body2TextStyle),
                keyboardType: TextInputType.emailAddress,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 32),
                child: Text(
                  'Ganti Password',
                  style: style.Display1TextStyle,
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                    labelText: 'Password baru',
                    labelStyle: style.Body2TextStyle),
                obscureText: true,
                keyboardType: TextInputType.visiblePassword,
              ),
              TextFormField(
                decoration: InputDecoration(
                    labelText: 'Ulangi Password',
                    labelStyle: style.Body2TextStyle),
                obscureText: true,
                keyboardType: TextInputType.visiblePassword,
              ),
              SizedBox(height: 24),
              Container(
                height: 40,
                width: double.infinity,
                child: FlatButton(
                    onPressed: () {},
                    child: Text('SIMPAN', style: style.ButtonTextStyle),
                    color: style.accentColor),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
