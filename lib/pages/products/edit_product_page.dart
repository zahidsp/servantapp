import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:servant_app/styles.dart' as style;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class EditProdukPage extends StatefulWidget {
  final List list;
  final int index;

  EditProdukPage({this.list, this.index});

  @override
  _EditProdukPageState createState() => _EditProdukPageState();
}

class _EditProdukPageState extends State<EditProdukPage> {
  TextEditingController controllerName;
  TextEditingController controllerHarga;
  TextEditingController controllerKategori;
  String imageFile;
  File _image;
  bool _isEdited = false;
  String _catValue;

  @override
  void initState() {
    controllerName =
        TextEditingController(text: widget.list[widget.index]['nama']);
    _catValue = widget.list[widget.index]['kategori'];
    controllerHarga = TextEditingController(
        text: widget.list[widget.index]['harga'].toString());
    imageFile = widget.list[widget.index]['picture'];
    super.initState();
  }

  Future _imageGalerry() async {
    var _imageFile;

    try {
      _imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    } catch (e) {
      print(e);
    }

    setState(() {
      _image = _imageFile;
    });
  }

  Future _imageCamera() async {
    var _imageFile;
    try {
      _imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    } catch (e) {
      print(e);
    }
    setState(() {
      _image = _imageFile;
    });
  }

  void _confirm() {
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        height: 146,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: Text(
                'Choose Action',
                style: TextStyle(
                    color: Colors.deepPurple[900],
                    fontWeight: FontWeight.w700,
                    fontSize: 18),
              ),
            ),
            ListTile(
              onTap: () {
                _imageCamera();
                _isEdited = true;
                Navigator.pop(context);
              },
              leading: CircleAvatar(
                child: Icon(
                  FontAwesomeIcons.camera,
                  color: Colors.white,
                ),
                backgroundColor: Colors.purpleAccent,
              ),
              title: Text('Camera'),
            ),
            ListTile(
              onTap: () {
                _imageGalerry();
                _isEdited = true;
                Navigator.pop(context);
              },
              leading: CircleAvatar(
                child: Icon(
                  FontAwesomeIcons.folder,
                  color: Colors.white,
                ),
                backgroundColor: Colors.pinkAccent,
              ),
              title: Text('Gallery'),
            )
          ],
        ),
      ),
    );

    showDialog(context: context, child: alertDialog);
  }

  static const catItem = <String>['KAOS', 'JAKET', 'KEMEJA', 'BLAZER', 'TOPI'];

  final List<DropdownMenuItem<String>> _dropDownItems = catItem
      .map(
        (String value) => DropdownMenuItem(
          value: value,
          child: Text(
            value,
            style: style.Body2TextStyle,
          ),
        ),
      )
      .toList();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Edit Data Produk'),
        ),
        body: Container(
          padding: const EdgeInsets.all(16),
          child: ListView(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(bottom: 0),
                padding: const EdgeInsets.symmetric(horizontal: 16),
                color: Colors.white,
                width: double.infinity,
                child: DropdownButton(
                  underline: Container(color: Colors.transparent),
                  isExpanded: true,
                  value: _catValue,
                  hint: Text(
                    'Semua Kategori',
                    style: style.Body2TextStyle,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      _catValue = newValue;
                    });
                  },
                  items: _dropDownItems,
                ),
              ),
              TextFormField(
                controller: controllerName,
                textCapitalization: TextCapitalization.words,
                decoration: InputDecoration(
                    labelText: 'Nama Produk', labelStyle: style.Body2TextStyle),
              ),
              TextFormField(
                controller: controllerHarga,
                decoration: InputDecoration(
                    labelText: 'Harga Satuan', labelStyle: style.Body2TextStyle),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: 14),
              Container(
                // margin: const EdgeInsets.only(top: 22),
                // height: 120.0,
                width: double.infinity,
                // color: Colors.black12,
                child: Center(
                    child: _image == null
                        ? Center(
                            child: Stack(
                              alignment: AlignmentDirectional.topEnd,
                              children: <Widget>[
                                Container(
                                    height: 240,
                                    width: double.infinity,
                                    child: Image.asset(imageFile,
                                        fit: BoxFit.cover)),
                                Container(
                                  height: 40,
                                  width: 110,
                                  color: Colors.black12.withOpacity(0.5),
                                  child: FlatButton.icon(
                                    onPressed: () {
                                      _confirm();
                                    },
                                    icon: Icon(
                                      FontAwesomeIcons.pencilAlt,
                                      color: Colors.white,
                                      size: 10,
                                    ),
                                    label: Text(
                                      'Edit Image',
                                      style: style.Body2TextStyle.copyWith(
                                          fontSize: 12, color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        : Center(
                            child: Stack(
                              alignment: AlignmentDirectional.topEnd,
                              children: <Widget>[
                                Image.file(_image),
                                Container(
                                  height: 40,
                                  width: 120,
                                  color: Colors.black12.withOpacity(0.3),
                                  child: FlatButton.icon(
                                    onPressed: () {
                                      _confirm();
                                    },
                                    icon: Icon(
                                      FontAwesomeIcons.pencilAlt,
                                      color: Colors.white,
                                      size: 10,
                                    ),
                                    label: Text(
                                      'Edit Image',
                                      style: style.Body2TextStyle.copyWith(
                                          fontSize: 12,color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
              ),
              SizedBox(height: 14),
              Container(
                height: 40,
                width: double.infinity,
                child: FlatButton(
                    onPressed: () {},
                    child: Text('SIMPAN', style: style.ButtonTextStyle),
                    color: style.accentColor),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
