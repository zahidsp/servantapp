import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:servant_app/styles.dart' as style;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//own import
import 'package:servant_app/pages/products/edit_product_page.dart';

class DetailProdukPage extends StatefulWidget {
  final List list;
  final int index;

  DetailProdukPage({this.list, this.index});

  @override
  _DetailProdukPageState createState() => _DetailProdukPageState();
}

class _DetailProdukPageState extends State<DetailProdukPage> {
  String imageFile;
  File _image;
  bool _isEdited = false;

  @override
  void initState() {
    imageFile = widget.list[widget.index]['picture'];
    super.initState();
  }

  void _delete() {
    AlertDialog alertDialog = new AlertDialog(
      content: Container(
        child: ListTile(
          leading: CircleAvatar(
            child: Icon(
              FontAwesomeIcons.trashAlt,
              color: Colors.white,
            ),
            backgroundColor: Colors.redAccent,
          ),
          title: Text(
              "Are you sure want to delete '${widget.list[widget.index]['nama']}' ?"),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {},
          color: Colors.redAccent,
          child: Text('Yes, Delete!',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: Colors.white,
                fontSize: 14,
              )),
        ),
        OutlineButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('No, Cancel',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: Colors.black45,
                fontSize: 14,
              )),
        )
      ],
    );

    showDialog(context: context, child: alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Detail Produk'),
        ),
        body: Container(
          padding: const EdgeInsets.all(16),
          child: ListView(
            children: <Widget>[
              Container(
                color: Colors.white,
                width: double.infinity,
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text(widget.list[widget.index]['kategori'],
                        style: style.Body1TextStyle.copyWith(
                            color: style.accentColor)),
                    Text(widget.list[widget.index]['nama'],
                        style: style.Display1TextStyle),
                    SizedBox(height: 8),
                    Text(widget.list[widget.index]['harga'],
                        style: style.Body2TextStyle),
                    SizedBox(height: 8),
                    Text("${widget.list[widget.index]['counter']}" + " Terjual",
                        style: style.Body3TextStyle),
                  ],
                ),
              ),
              SizedBox(height: 14),
              Container(
                // margin: const EdgeInsets.only(top: 22),
                // height: 120.0,
                width: double.infinity,
                // color: Colors.black12,
                child: Center(
                    child: _image == null
                        ? Center(
                            child: Container(
                                height: 240,
                                width: double.infinity,
                                child:
                                    Image.asset(imageFile, fit: BoxFit.cover)),
                          )
                        : Center(
                            child: Stack(
                              alignment: AlignmentDirectional.topEnd,
                              children: <Widget>[
                                Image.file(_image),
                                Container(
                                  height: 40,
                                  width: 120,
                                  color: Colors.black12.withOpacity(0.3),
                                  child: FlatButton.icon(
                                    onPressed: () {},
                                    icon: Icon(
                                      FontAwesomeIcons.pencilAlt,
                                      color: Colors.white,
                                      size: 10,
                                    ),
                                    label: Text(
                                      'Edit Image',
                                      style: style.Body2TextStyle.copyWith(
                                          fontSize: 12, color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
              ),
              SizedBox(height: 14),
              Container(
                height: 120,
                color: Colors.white,
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        width: double.infinity,
                        child: FlatButton.icon(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EditProdukPage(
                                        list: widget.list,
                                        index: widget.index)));
                          },
                          label: Text(
                            'Edit',
                            style: style.Body1TextStyle.copyWith(
                                color: style.darkBlueIcon),
                          ),
                          icon: Icon(
                            FontAwesomeIcons.pencilAlt,
                            size: 18,
                            color: style.darkBlueIcon,
                          ),
                        ),
                      ),
                    ),
                    Divider(color: style.line),
                    Expanded(
                      child: Container(
                        width: double.infinity,
                        child: FlatButton.icon(
                          onPressed: () {
                            _delete();
                          },
                          label: Text(
                            'Hapus',
                            style: style.Body1TextStyle.copyWith(
                                color: style.redIcon),
                          ),
                          icon: Icon(FontAwesomeIcons.trashAlt,
                              size: 18, color: style.redIcon),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
