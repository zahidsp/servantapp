import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//own import
import 'package:servant_app/components/app_drawer.dart';
import 'package:servant_app/pages/products/edit_product_page.dart';
import 'package:servant_app/pages/products/add_product_page.dart';
import 'package:servant_app/pages/products/detail_product_page.dart';

class ProductPage extends StatefulWidget {
  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  String _catValue;
  static const catItem = <String>['KAOS', 'JAKET', 'KEMEJA', 'BLAZER', 'TOPI'];

  final List<DropdownMenuItem<String>> _dropDownItems = catItem
      .map(
        (String value) => DropdownMenuItem(
          value: value,
          child: Text(
            value,
            style: style.Body2TextStyle,
          ),
        ),
      )
      .toList();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Produk'),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(FontAwesomeIcons.search),
            iconSize: 18,
            color: Colors.white,
          )
        ],
      ),
      drawer: Drawer(
        child: AppDrawer(),
      ),
      body: ListView(
        // padding: const EdgeInsets.only(top: 14),
        children: <Widget>[
          Container(
            margin: const EdgeInsets.fromLTRB(16, 16, 16, 0),
            padding: const EdgeInsets.symmetric(horizontal: 16),
            color: Colors.white,
            width: double.infinity,
            child: DropdownButton(
              underline: Container(color: Colors.transparent),
              isExpanded: true,
              value: _catValue,
              hint: Text(
                'Semua Kategori',
                style: style.Body2TextStyle,
              ),
              onChanged: (String newValue) {
                setState(() {
                  _catValue = newValue;
                });
              },
              items: _dropDownItems,
            ),
          ),
          ProductList(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>AddProdukPage()));
        },
        backgroundColor: style.accentColor,
        child: Icon(
          FontAwesomeIcons.plus,
          color: style.TextColorDark,
          size: 18,
        ),
      ),
    );
  }
}

class ProductList extends StatefulWidget {
  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  int _index;
  void _action() {
    showBottomSheet(
        context: context,
        builder: (BuildContext context) => Container(
              padding: const EdgeInsets.all(12),
              height: 160,
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      child: FlatButton.icon(
                        onPressed: () {
                          Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EditProdukPage(
                            list: productList,
                            index: _index,
                          )));
                        },
                        label: Text(
                          'Edit',
                          style: style.Body1TextStyle.copyWith(
                              color: style.darkBlueIcon),
                        ),
                        icon: Icon(
                          FontAwesomeIcons.pencilAlt,
                          size: 18,
                          color: style.darkBlueIcon,
                        ),
                      ),
                    ),
                  ),
                  Divider(color: style.line),
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      child: FlatButton.icon(
                        onPressed: () {},
                        label: Text(
                          'Hapus',
                          style: style.Body1TextStyle.copyWith(
                              color: style.redIcon),
                        ),
                        icon: Icon(FontAwesomeIcons.trashAlt,
                            size: 18, color: style.redIcon),
                      ),
                    ),
                  )
                ],
              ),
            ));
  }

  var productList = [
    {
      'nama': 'Kaos Oblong',
      'harga': '55000',
      'picture': 'assets/images/products/w3.jpeg',
      'kategori': 'KAOS',
      'counter': '1857',
    },
    {
      'nama': 'Jaket Parachute ',
      'harga': '150000',
      'picture': 'assets/images/products/w4.jpeg',
      'kategori': 'JAKET',
      'counter': '753',
    },
    {
      'nama': 'Blazer Outdoor',
      'harga': '205000',
      'picture': 'assets/images/products/blazer2.jpeg',
      'kategori': 'BLAZER',
      'counter': '457',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: productList.length,
        itemBuilder: (BuildContext context, int index) {
          _index = index;
          return Container(
              child: ListTile(
            onTap: () {
               Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DetailProdukPage(
                            list: productList,
                            index: index,
                          )));
            },
            onLongPress: () {
              _action();
            },
            leading: Container(
              width: 42,
              height: 42,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image.asset(
                  productList[index]['picture'],
                  fit: BoxFit.cover,
                ),
              ),
            ),
            title: Text(
              productList[index]['kategori'],
              style: style.Body1TextStyle.copyWith(
                  color: style.accentColor, fontSize: 12),
            ),
            subtitle: Text(
              productList[index]['nama'],
              style: style.Display2TextStyle,
            ),
            trailing: Column(
              children: <Widget>[
                SizedBox(
                  height: 36,
                ),
                Text(
                  "${productList[index]['counter']}" + " Terjual",
                  style: style.Body3TextStyle.copyWith(fontSize: 12),
                ),
              ],
            ),
          ));
        });
  }
}
