import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;

class TabelSelesai extends StatefulWidget {
  @override
  _TabelSelesaiState createState() => _TabelSelesaiState();
}

class _TabelSelesaiState extends State<TabelSelesai> {
  var listOrder = [
    {
      'name': 'Kaos Oke',
      'total': '8200000',
      'produksi': '4000000',
      'lunas': '4200000',
      'profit': '4200000'
    },
    {
      'name': 'Jaket Jos',
      'total': '5600000',
      'produksi': '2000000',
      'lunas': '3600000',
      'profit': '3600000'
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(children: [
      Container(
        padding: const EdgeInsets.all(8),
        color: style.TextColor2,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
                child: Center(
                    child: Text(
              'Order',
              style: style.Body1TextStyle.copyWith(color: style.accentColor, fontSize: 14),
            ))),
            Expanded(
                child: Center(
                    child: Text(
              'Total',
              style: style.Body1TextStyle.copyWith(color: style.accentColor, fontSize: 14),
            ))),
            Expanded(
                child: Center(
                    child: Text(
              'Produksi',
              style: style.Body1TextStyle.copyWith(color: style.accentColor, fontSize: 14),
            ))),
            Expanded(
                child: Center(
                    child: Text(
              'Lunas',
              style: style.Body1TextStyle.copyWith(color: style.accentColor, fontSize: 14),
            ))),
            Expanded(
                child: Center(
                    child: Text(
              'Profit',
              style: style.Body1TextStyle.copyWith(color: style.accentColor, fontSize: 14),
            ))),
          ],
        ),
      ),
      ListView.builder(
        shrinkWrap: true,
        itemCount: listOrder.length,
        itemBuilder: (BuildContext context, int i) {
          return SingleOrder(
            orderName: listOrder[i]['name'],
            orderTotal: listOrder[i]['total'],
            orderProduksi: listOrder[i]['produksi'],
            orderLunas: listOrder[i]['lunas'],
            orderProfit: listOrder[i]['profit'],
          );
        },
      )
    ]));
  }
}

class SingleOrder extends StatelessWidget {
  final orderName;
  final orderTotal;
  final orderProduksi;
  final orderProfit;
  final orderLunas;

  SingleOrder(
      {this.orderName,
      this.orderTotal,
      this.orderProduksi,
      this.orderProfit,
      this.orderLunas});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 4),
      decoration:
          BoxDecoration(border: Border(bottom: BorderSide(color: style.line))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Expanded(
              child: Center(
                  child: Text(
            '$orderName',
            style: style.Body2TextStyle.copyWith(fontSize: 12),
          ))),
          Expanded(
              child: Center(
                  child: Text(
            '$orderTotal',
            style: style.Body2TextStyle.copyWith(fontSize: 12),
          ))),
          Expanded(
              child: Center(
                  child: Text(
            '$orderProduksi',
            style: style.Body2TextStyle.copyWith(fontSize: 12),
          ))),
          Expanded(
              child: Center(
                  child: Text(
            '$orderLunas',
            style: style.Body2TextStyle.copyWith(fontSize: 12),
          ))),
          Expanded(
              child: Center(
                  child: Text(
            '$orderProfit',
            style: style.Body2TextStyle.copyWith(fontSize: 12),
          ))),
        ],
      ),
    );
  }
}
