import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;

class TabelProduksi extends StatefulWidget {
  @override
  _TabelProduksiState createState() => _TabelProduksiState();
}

class _TabelProduksiState extends State<TabelProduksi> {
  var listOrder = [
    {
      'name': 'Kaos Oke',
      'total': '8200000',
      'produksi': '4000000',
      'profit': '4200000'
    },
    {
      'name': 'Jaket Jos',
      'total': '5600000',
      'produksi': '2000000',
      'profit': '3600000'
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(children: [
      Container(
        padding: const EdgeInsets.all(6),
        color: style.TextColor2,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
                child: Center(
                    child: Text(
              'Order',
              style: style.Body1TextStyle.copyWith(color: style.accentColor),
            ))),
            Expanded(
                child: Center(
                    child: Text(
              'Total',
              style: style.Body1TextStyle.copyWith(color: style.accentColor),
            ))),
            Expanded(
                child: Center(
                    child: Text(
              'Produksi',
              style: style.Body1TextStyle.copyWith(color: style.accentColor),
            ))),
            Expanded(
                child: Center(
                    child: Text(
              'Profit',
              style: style.Body1TextStyle.copyWith(color: style.accentColor),
            ))),
          ],
        ),
      ),
      ListView.builder(
        shrinkWrap: true,
        itemCount: listOrder.length,
        itemBuilder: (BuildContext context, int i) {
          return SingleOrder(
            orderName: listOrder[i]['name'],
            orderTotal: listOrder[i]['total'],
            orderProduksi: listOrder[i]['produksi'],
            orderProfit: listOrder[i]['profit'],
          );
        },
      )
    ]));
  }
}

class SingleOrder extends StatelessWidget {
  final orderName;
  final orderTotal;
  final orderProduksi;
  final orderProfit;

  SingleOrder(
      {this.orderName, this.orderTotal, this.orderProduksi, this.orderProfit});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 4),
      decoration:
          BoxDecoration(border: Border(bottom: BorderSide(color: style.line))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Expanded(
              child: Center(
                  child: Text(
            '$orderName',
            style: style.Body2TextStyle,
          ))),
          Expanded(
              child: Center(
                  child: Text(
            '$orderTotal',
            style: style.Body2TextStyle,
          ))),
          Expanded(
              child: Center(
                  child: Text(
            '$orderProduksi',
            style: style.Body2TextStyle,
          ))),
          Expanded(
              child: Center(
                  child: Text(
            '$orderProfit',
            style: style.Body2TextStyle,
          ))),
        ],
      ),
    );
  }
}
