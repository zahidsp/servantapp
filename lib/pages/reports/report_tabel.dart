import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;

class TabelOrder extends StatefulWidget {
  @override
  _TabelOrderState createState() => _TabelOrderState();
}

class _TabelOrderState extends State<TabelOrder> {
  var listOrder = [
    {
      'name': 'Kaos Oke',
      'total': '8200000',
      'bayar': '4000000',
      'kekurangan': '4200000'
    },
    {
      'name': 'Jaket Jos',
      'total': '5600000',
      'bayar': '2000000',
      'kekurangan': '3600000'
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(children: [
      Container(
        padding: const EdgeInsets.all(6),
        color: style.TextColor2,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
                child: Center(
                    child: Text(
              'Order',
              style: style.Body1TextStyle.copyWith(color: style.accentColor),
            ))),
            Expanded(
                child: Center(
                    child: Text(
              'Total',
              style: style.Body1TextStyle.copyWith(color: style.accentColor),
            ))),
            Expanded(
                child: Center(
                    child: Text(
              'DP',
              style: style.Body1TextStyle.copyWith(color: style.accentColor),
            ))),
            Expanded(
                child: Center(
                    child: Text(
              'Kurang',
              style: style.Body1TextStyle.copyWith(color: style.accentColor),
            ))),
          ],
        ),
      ),
      ListView.builder(
        shrinkWrap: true,
        itemCount: listOrder.length,
        itemBuilder: (BuildContext context, int i) {
          return SingleOrder(
            orderName: listOrder[i]['name'],
            orderTotal: listOrder[i]['total'],
            orderBayar: listOrder[i]['bayar'],
            orderKurang: listOrder[i]['kekurangan'],
          );
        },
      )
    ]));
  }
}

class SingleOrder extends StatelessWidget {
  final orderName;
  final orderTotal;
  final orderBayar;
  final orderKurang;

  SingleOrder(
      {this.orderName, this.orderTotal, this.orderBayar, this.orderKurang});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 4),
      decoration:
          BoxDecoration(border: Border(bottom: BorderSide(color: style.line))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Expanded(
              child: Center(
                  child: Text(
            '$orderName',
            style: style.Body2TextStyle,
          ))),
          Expanded(
              child: Center(
                  child: Text(
            '$orderTotal',
            style: style.Body2TextStyle,
          ))),
          Expanded(
              child: Center(
                  child: Text(
            '$orderBayar',
            style: style.Body2TextStyle,
          ))),
          Expanded(
              child: Center(
                  child: Text(
            '$orderKurang',
            style: style.Body2TextStyle,
          ))),
        ],
      ),
    );
  }
}
