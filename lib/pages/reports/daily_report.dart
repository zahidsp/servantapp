import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

//own import
import 'package:servant_app/components/app_drawer.dart';
import 'package:servant_app/pages/reports/report_tabel.dart';
import 'package:servant_app/pages/reports/report_tabel_produksi.dart';
import 'package:servant_app/pages/reports/report_tabel_selesai.dart';

class DailyReportPage extends StatefulWidget {
  @override
  _DailyReportPageState createState() => _DailyReportPageState();
}

class _DailyReportPageState extends State<DailyReportPage> {
  DateTime _tanggalOrder = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Laporan Harian'),
          actions: <Widget>[
            IconButton(
              onPressed: () {},
              icon: Icon(FontAwesomeIcons.save),
              iconSize: 18,
              color: Colors.white,
            )
          ],
        ),
        drawer: Drawer(
          child: AppDrawer(),
        ),
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(bottom: 14),
              color: Colors.white,
              child: ListTile(
                title: Text('Tanggal', style: style.Body1TextStyle.copyWith(color: style.accentColor)),
                subtitle: Padding(
                  padding: const EdgeInsets.only(top: 6.0),
                  child: Text('${_tanggalOrder.toLocal()}',
                      style: style.Display2TextStyle),
                ),
                onTap: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(2017, 1, 1),
                      maxTime: DateTime(2021, 12, 31), onConfirm: (date) {
                    setState(() {
                      _tanggalOrder = date;
                    });
                  });
                },
              ),
            ),
            Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      color: Colors.white,
                      child: ListTile(
                        title: Text('Pemasukkan', style: style.Body1TextStyle.copyWith(color: style.accentColor),),
                        subtitle: Text('4500000', style: style.Display2TextStyle,),
                      ),
                    ),
                  ),
                  SizedBox(width: 14),
                  Expanded(
                    child: Container(
                      color: Colors.white,
                      child: ListTile(
                        title: Text('Pengeluaran', style: style.Body1TextStyle.copyWith(color: style.accentColor),),
                        subtitle: Text('4500000', style: style.Display2TextStyle,),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 14.0, bottom: 8),
                child: Text(
                  'Transaksi Order',
                  style: style.Display1TextStyle,
                )),
            TabelOrder(),
            Padding(
                padding: const EdgeInsets.only(top: 14.0, bottom: 8),
                child: Text(
                  'Beban Produksi',
                  style: style.Display1TextStyle,
                )),
            TabelProduksi(),
            Padding(
                padding: const EdgeInsets.only(top: 14.0, bottom: 8),
                child: Text(
                  'Order Selesai',
                  style: style.Display1TextStyle,
                )),
                TabelSelesai(),
          ],
        ),
      ),
    );
  }
}
