import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//own import
import 'package:servant_app/styles.dart' as style;
import 'package:shared_preferences/shared_preferences.dart';
import '../../components/app_drawer.dart';
import '../../pages/orders/create/order_baru_page.dart';
import '../../pages/customers/customer_choose_page.dart';
import 'package:servant_app/pages/profiles/edit_profile_page.dart';

class HomePage extends StatefulWidget {
  final VoidCallback signOut;

  HomePage({this.signOut});
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String nama, email, picture;

  signOut() {
    setState(() {
      widget.signOut();
    });
  }

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      nama = preferences.getString("nama");
      email = preferences.getString("email");
      picture = preferences.getString("picture");
    });
  }

  void profil() {
    AlertDialog alertDialog = new AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      content: Container(
        height: 400,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
                height: 160,
                width: double.infinity,
                child: Image.asset(
                  'assets/images/products/m2.jpg',
                  fit: BoxFit.cover,
                )),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(16),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("$nama",
                      style: style.TitleTextStyle.copyWith(
                          color: style.accentColor)),
                  SizedBox(
                    height: 8,
                  ),
                  Text("$email", style: style.Body3TextStyle),
                  SizedBox(
                    height: 8,
                  ),
                  Text("$picture", style: style.Body2TextStyle),
                ],
              ),
            ),
            // SizedBox(height: 42),
            InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => EditPage()));
              },
              child: Container(
                height: 38,
                width: double.infinity,
                color: Colors.white,
                child: Center(
                  child: Text(
                    'EDIT PROFIL',
                    style: style.ButtonTextStyle,
                  ),
                ),
              ),
            ),
            Container(
              color: style.line,
              height: 1,
            ),
            InkWell(
              onTap: () {
                signOut();
                Navigator.pop(context);
              },
              child: Container(
                height: 38,
                width: double.infinity,
                color: Colors.white,
                child: Center(
                  child: Text(
                    'LOGOUT',
                    style: style.ButtonTextStyle,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );

    showDialog(context: context, child: alertDialog);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Servant Factory'),
        actions: <Widget>[
          InkWell(
            onTap: () {
              profil();
            },
            child: Container(
              height: 44,
              width: 58,
              margin: const EdgeInsets.only(right: 8, left: 8),
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                  // borderRadius: BorderRadius.all(Radius.circular(28.0)),
                  shape: BoxShape.circle),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image.asset(
                  'assets/images/products/m2.jpg',
                  fit: BoxFit.cover,
                ),
              ),
            ),
          )
        ],
      ),
      drawer: Drawer(
        child: AppDrawer(),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: ListView(
          primary: false,
          padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16),
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: InkWell(
                    onTap: () {},
                    child: Container(
                      padding: const EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(18.0)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 28.0),
                            child: Container(
                              height: 38.0,
                              width: 38.0,
                              decoration: new BoxDecoration(
                                color: style.bgGreen,
                                shape: BoxShape.circle,
                                // border:
                                //     Border.all(color: style.primaryColorLight),
                              ),
                              child: Icon(
                                FontAwesomeIcons.balanceScale,
                                color: style.greenIcon,
                                size: 18,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 2, 0, 4),
                            child: Text(
                              'Rp6.750.000',
                              style: style.Display2TextStyle,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: Text(
                              'Saldo',
                              style: style.Body3TextStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: InkWell(
                    onTap: () {},
                    child: Container(
                      padding: const EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(18.0)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 28.0),
                            child: Container(
                              height: 38.0,
                              width: 38.0,
                              decoration: new BoxDecoration(
                                color: style.bgDarkBlue,
                                shape: BoxShape.circle,
                                // border:
                                //     Border.all(color: style.primaryColorLight),
                              ),
                              child: Icon(
                                FontAwesomeIcons.handshake,
                                color: style.darkBlueIcon,
                                size: 18,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 2, 0, 4),
                            child: Text(
                              '375',
                              style: style.Display2TextStyle,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: Text(
                              'Order',
                              style: style.Body3TextStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              children: <Widget>[
                Expanded(
                  child: InkWell(
                    onTap: () {},
                    child: Container(
                      padding: const EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(18.0)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 28.0),
                            child: Container(
                              height: 38.0,
                              width: 38.0,
                              decoration: new BoxDecoration(
                                color: style.bgPurple,
                                shape: BoxShape.circle,
                                // border:
                                //     Border.all(color: style.primaryColorLight),
                              ),
                              child: Icon(
                                FontAwesomeIcons.moneyBillWave,
                                color: style.purpleIcon,
                                size: 18,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 2, 0, 4),
                            child: Text(
                              'Rp6.750.000',
                              style: style.Display2TextStyle,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: Text(
                              'Nilai Order',
                              style: style.Body3TextStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: InkWell(
                    onTap: () {},
                    child: Container(
                      padding: const EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(18.0)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 28.0),
                            child: Container(
                              height: 38.0,
                              width: 38.0,
                              decoration: new BoxDecoration(
                                color: style.bgDarkPurple,
                                shape: BoxShape.circle,
                                // border:
                                //     Border.all(color: style.primaryColorLight),
                              ),
                              child: Icon(
                                FontAwesomeIcons.cartPlus,
                                color: style.darkPurpleIcon,
                                size: 18,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 2, 0, 4),
                            child: Text(
                              'Rp5.550.000',
                              style: style.Display2TextStyle,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: Text(
                              'Beban Produksi',
                              style: style.Body3TextStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              children: <Widget>[
                Expanded(
                  child: InkWell(
                    onTap: () {},
                    child: Container(
                      padding: const EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(18.0)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 28.0),
                            child: Container(
                              height: 38.0,
                              width: 38.0,
                              decoration: new BoxDecoration(
                                color: style.bgOrange,
                                shape: BoxShape.circle,
                                // border:
                                //     Border.all(color: style.primaryColorLight),
                              ),
                              child: Icon(
                                FontAwesomeIcons.users,
                                color: style.orangeIcon,
                                size: 18,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 2, 0, 4),
                            child: Text(
                              '475',
                              style: style.Display2TextStyle,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: Text(
                              'Klien',
                              style: style.Body3TextStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: InkWell(
                    onTap: () {},
                    child: Container(
                      padding: const EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(18.0)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 28.0),
                            child: Container(
                              height: 38.0,
                              width: 38.0,
                              decoration: new BoxDecoration(
                                color: style.bgBlue,
                                shape: BoxShape.circle,
                                // border:
                                //     Border.all(color: style.primaryColorLight),
                              ),
                              child: Icon(
                                FontAwesomeIcons.storeAlt,
                                color: style.blueIcon,
                                size: 18,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 2, 0, 4),
                            child: Text(
                              '72',
                              style: style.Display2TextStyle,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: Text(
                              'Suplier',
                              style: style.Body3TextStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: SpeedDial(
        animatedIconTheme: IconThemeData(
          color: style.primaryColor,
        ),
        animatedIcon: AnimatedIcons.add_event,
        backgroundColor: style.accentColor,
        children: [
          SpeedDialChild(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CustomerChoosePage()));
              },
              label: 'Order Baru',
              labelStyle: style.Body2TextStyle,
              backgroundColor: style.darkBlueIcon,
              child: Icon(
                FontAwesomeIcons.handshake,
                size: 18,
                color: Colors.white,
              )),
          SpeedDialChild(
              onTap: () {},
              label: 'Produksi',
              labelStyle: style.Body2TextStyle,
              backgroundColor: style.darkPurpleIcon,
              child: Icon(
                FontAwesomeIcons.cartPlus,
                size: 18,
                color: Colors.white,
              )),
          SpeedDialChild(
              onTap: () {},
              label: 'Klien Baru',
              labelStyle: style.Body2TextStyle,
              backgroundColor: style.orangeIcon,
              child: Icon(
                FontAwesomeIcons.users,
                size: 18,
                color: Colors.white,
              )),
          SpeedDialChild(
              onTap: () {},
              label: 'Suplier Baru',
              labelStyle: style.Body2TextStyle,
              backgroundColor: style.blueIcon,
              child: Icon(
                FontAwesomeIcons.storeAlt,
                size: 18,
                color: Colors.white,
              ))
        ],
      ),
    );
  }
}
