import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;

//styles
import 'styles.dart';

//import page
import 'package:servant_app/pages/auth/login_page.dart';

void main() => runApp(MaterialApp(
    debugShowCheckedModeBanner: false, theme: _themeData(), home: LoginPage()));

ThemeData _themeData() {
  return ThemeData(
    appBarTheme: AppBarTheme(
      elevation: 0.0,
      textTheme: TextTheme(title: AppBarTextStyle),
    ),
    textTheme: TextTheme(
        title: TitleTextStyle,
        headline: HeadTextStyle,
        display1: Display1TextStyle,
        display2: Display2TextStyle,
        body1: Body1TextStyle,
        body2: Body2TextStyle,
        caption: style.Body3TextStyle,
        button: ButtonTextStyle),
    primaryColor: style.primaryColor,
    accentColor: style.accentColor,
    scaffoldBackgroundColor: style.backgroundColor,
  );
}
