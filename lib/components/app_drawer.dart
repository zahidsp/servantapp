import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:servant_app/styles.dart' as style;

//import page
import 'package:servant_app/pages/homes/home_page.dart';
import 'package:servant_app/pages/clients/client_page.dart';
import 'package:servant_app/pages/reports/daily_report.dart';
import 'package:servant_app/pages/orders/order_page.dart';
import 'package:servant_app/pages/products/product_page.dart';
import 'package:servant_app/pages/categories/category_page.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: style.primaryColor,
      child: ListView(
        children: <Widget>[
          Container(
            height: 150,
            color: style.primaryColor,
            padding: const EdgeInsets.fromLTRB(16, 10, 16, 16),
            child: Column(
              children: <Widget>[
                Container(
                    padding: const EdgeInsets.all(16),
                    child:
                        Image.asset('assets/images/servant/servantFull.png')),
                Text(
                  'Jalan Wonosari km 11, Piyungan, Bantul',
                  style: style.Body3TextStyle.copyWith(color: Colors.white),
                )
              ],
            ),
          ),

          // drawer body
          Container(
            color: style.backgroundColor,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 12,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ListTile(
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomePage()));
                      },
                      title: Text(
                        'Dashboard',
                        style: style.Body2TextStyle,
                      ),
                      leading: Icon(FontAwesomeIcons.home,
                          color: style.accentColor, size: 22),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => OrderPage()));
                      },
                      title: Text('Order', style: style.Body2TextStyle),
                      leading: Icon(FontAwesomeIcons.handshake,
                          color: style.accentColor, size: 22),
                    ),
                    ListTile(
                      onTap: () {},
                      title:
                          Text('Beban Produksi', style: style.Body2TextStyle),
                      leading: Icon(FontAwesomeIcons.cartPlus,
                          color: style.accentColor, size: 22),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ClientPage()));
                      },
                      title: Text('Klien', style: style.Body2TextStyle),
                      leading: Icon(FontAwesomeIcons.users,
                          color: style.accentColor, size: 22),
                    ),
                    ListTile(
                      onTap: () {},
                      title: Text('Suplier', style: style.Body2TextStyle),
                      leading: Icon(FontAwesomeIcons.storeAlt,
                          color: style.accentColor, size: 22),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DailyReportPage()));
                      },
                      title:
                          Text('Laporan Harian', style: style.Body2TextStyle),
                      leading: Icon(FontAwesomeIcons.receipt,
                          color: style.accentColor, size: 22),
                    ),
                    ListTile(
                      onTap: () {},
                      title:
                          Text('Laporan Bulanan', style: style.Body2TextStyle),
                      leading: Icon(FontAwesomeIcons.book,
                          color: style.accentColor, size: 22),
                    ),
                    ListTile(
                      onTap: () {
                         Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProductPage()));
                      },
                      title: Text('Produk', style: style.Body2TextStyle),
                      leading: Icon(FontAwesomeIcons.tshirt,
                          color: style.accentColor, size: 22),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CategoryPage()));
                      },
                      title:
                          Text('Kategori Produk', style: style.Body2TextStyle),
                      leading: Icon(
                        FontAwesomeIcons.shapes,
                        color: style.accentColor,
                        size: 22,
                      ),
                    ),
                    Container(
                      height: 24,
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
