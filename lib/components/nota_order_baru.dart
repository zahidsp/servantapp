import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;

class NotaProduct extends StatefulWidget {
  @override
  _NotaProductState createState() => _NotaProductState();
}

class _NotaProductState extends State<NotaProduct> {
  int numbProd = 0;
  var productOnCart = [
    {
      'name': 'Blazer Two',
      'discount_price': 70000,
      'size': 'M',
      'color': 'Black',
      'qty': 2,
      'note': 'Parasit, Hitam',
      'final_price': 140000,
    },
    {
      'name': 'Kaos Oblong One',
      'discount_price': 80000,
      'size': 'Normal',
      'color': 'Red',
      'qty': 1,
      'note': 'Combed 20s, Merah',
      'final_price': 160000,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListView.builder(
          shrinkWrap: true,
          itemCount: productOnCart.length,
          itemBuilder: (BuildContext context, int index) {
            return SingleProductCart(
              cartProdNumb: this.numbProd += 1,
              cartProdName: productOnCart[index]['name'],
              cartProdNote: productOnCart[index]['note'],
              cartProdSize: productOnCart[index]['size'],
              cartProdColor: productOnCart[index]['color'],
              cartProdQty: productOnCart[index]['qty'],
              cartProdFinalPrice: productOnCart[index]['final_price'],
              cartProdPrice: productOnCart[index]['discount_price'],
            );
          },
        ),
        Container(
          padding: const EdgeInsets.only(top: 8),
          decoration: BoxDecoration(
              border: Border(top: BorderSide(color: style.line))),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                child: Text(
                  'Total Order',
                  style: style.Body2TextStyle,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                child: Text(
                  '3 Items',
                  style: style.Body2TextStyle
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                child: Text(
                  'Rp1900000',
                  style: style.Display2TextStyle
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class SingleProductCart extends StatelessWidget {
  final cartProdNumb;
  final cartProdName;
  final cartProdNote;
  final cartProdPrice;
  final cartProdSize;
  final cartProdColor;
  final cartProdQty;
  final cartProdFinalPrice;

  SingleProductCart(
      {this.cartProdNumb,
      this.cartProdName,
      this.cartProdNote,
      this.cartProdPrice,
      this.cartProdSize,
      this.cartProdColor,
      this.cartProdQty,
      this.cartProdFinalPrice});

  @override
  Widget build(BuildContext context) {
    return (Container(
      margin: const EdgeInsets.only(bottom: 8),
      // elevation: 0.0,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              padding: const EdgeInsets.only(left: 16),
              child: Text(
                '$cartProdNumb',
                style: style.Body2TextStyle,
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8.0, 0, 4.0),
                  child: Text(
                    cartProdName,
                    style: style.Body1TextStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: Text(
                    '$cartProdQty' + ' pcs X ' + '$cartProdPrice',
                    style: style.Body2TextStyle.copyWith(fontSize: 12),
                  ),
                ),
                // Padding(
                //   padding: const EdgeInsets.all(1.0),
                //   child: Text(
                //     'Warna $cartProdColor',
                //     style: TextStyle(color: Colors.black54, fontSize: 12),
                //   ),
                // ),
                // Padding(
                //   padding: const EdgeInsets.all(1.0),
                //   child: Text(
                //     'Ukuran $cartProdSize',
                //     style: TextStyle(color: Colors.black54, fontSize: 12),
                //   ),
                // ),
                Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: Text(
                    '$cartProdNote',
                    style: style.Body3TextStyle,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: Center(
              // padding: const EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),

              child: Text('\Rp $cartProdFinalPrice',
                  style: style.Display2TextStyle),
            ),
          ),
        ],
      ),
    ));
  }
}
