import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;

class Garis extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: style.line,
      height: 1,
      margin: const EdgeInsets.symmetric(vertical: 10),
    );
  }
}
