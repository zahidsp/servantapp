import 'package:flutter/material.dart';
import 'package:servant_app/styles.dart' as style;

class CartProduct extends StatefulWidget {
  @override
  _CartProductState createState() => _CartProductState();
}

class _CartProductState extends State<CartProduct> {
  var productOnCart = [
    {
      'name': 'Blazer Two',
      'picture': 'images/products/blazer2.jpeg',
      'discount_price': 1200000,
      'size': 'M',
      'color': 'Black',
      'qty': 2,
    },
    {
      'name': 'Dress One',
      'picture': 'images/products/dress1.jpeg',
      'discount_price': 700000,
      'size': 'M',
      'color': 'Red',
      'qty': 1,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListView.builder(
          shrinkWrap: true,
          itemCount: productOnCart.length,
          itemBuilder: (BuildContext context, int index) {
            return SingleProductCart(
              cartProdName: productOnCart[index]['name'],
              cartProdPict: productOnCart[index]['picture'],
              cartProdPrice: productOnCart[index]['discount_price'],
              cartProdSize: productOnCart[index]['size'],
              cartProdColor: productOnCart[index]['color'],
              cartProdQty: productOnCart[index]['qty'],
            );
          },
        ),
        SizedBox(height: 4.0),
        Container(
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                child: Text(
                  'Total Order',
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                child: Text(
                  '3 Items',
                  style: TextStyle(fontSize: 14),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                child: Text(
                  'Rp1900000',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      color: Colors.blue),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class SingleProductCart extends StatelessWidget {
  final cartProdName;
  final cartProdPict;
  final cartProdPrice;
  final cartProdSize;
  final cartProdColor;
  final cartProdQty;

  SingleProductCart(
      {this.cartProdName,
      this.cartProdPict,
      this.cartProdPrice,
      this.cartProdSize,
      this.cartProdColor,
      this.cartProdQty});

  @override
  Widget build(BuildContext context) {
    return (Card(
      elevation: 0.0,
      color: Colors.transparent,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              child: IconButton(
                icon: Icon(
                  Icons.delete_outline,
                  color: style.redIcon,
                ),
                onPressed: () {},
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8.0, 0, 1.0),
                  child: Text(
                    cartProdName,
                    style: style.Body1TextStyle,
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: Text(
                    '$cartProdQty' + ' pcs X ' + '$cartProdPrice',
                    style: style.Body2TextStyle,
                  ),
                ),

                // Padding(
                //   padding: const EdgeInsets.all(2.0),
                //   child: Text(
                //     'Ukuran $cartProdSize',
                //     style: style.Body2TextStyle,
                //   ),
                // ),

                Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: Text(
                    'Warna dongker, kain combed 30s',
                    style: style.Body3TextStyle,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: Center(
              // padding: const EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),

              child: Text('\Rp $cartProdPrice',
                  style: style.Display2TextStyle.copyWith(
                      color: style.accentColor)),
            ),
          )
        ],
      ),
    ));
  }
}
