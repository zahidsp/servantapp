import 'package:flutter/material.dart';

const largeText = 22.0;
const mediumText = 18.0;
const smallText = 16.0;
const thinText = 14.0;

const String myFont = 'Montserrat';

const Color primaryColor = Color(0xFF1F1F1F);
const Color accentColor = Color(0xFFFFC400);
const Color line = Color(0xFFE1E1E1);
const Color backgroundColor = Color(0xFFF6F6F6);

const Color TextColorDark = Color(0xFF363636);
const Color TextColor2 = Color(0xFF555555);
const Color TextColor1 = Color(0xFFA3A3A3);
const Color TextColorLight = Colors.white;

const Color blueIcon = Color(0xFF02AFFF);
const Color greenIcon = Color(0xFF3FBF5F);
const Color purpleIcon = Color(0xFF7C4DFF);
const Color darkPurpleIcon = Color(0xFFD500FA);
const Color redIcon = Color(0xFFF53B5F);
const Color orangeIcon = Color(0xFFFE6E41);
const Color darkBlueIcon = Color(0xFF2778FF);

const Color bgBlue = Color(0xFFEBF9FF);
const Color bgOrange = Color(0xFFFFEEE9);
const Color bgDarkPurple = Color(0xFFFCECFF);
const Color bgPurple = Color(0xFFF2EEFF);
const Color bgDarkBlue = Color(0xFFEFF5FF);
const Color bgGreen = Color(0xFFEEFFF1);








const AppBarTextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: largeText,
    fontWeight: FontWeight.w700,
    color: Colors.white);

const TitleTextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: largeText,
    fontWeight: FontWeight.w700,
    color: TextColorDark);

const HeadTextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: largeText,
    fontWeight: FontWeight.w900,
    color: TextColorDark);

const Display1TextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: largeText,
    fontWeight: FontWeight.w700,
    color: TextColorDark);

const Display2TextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: mediumText,
    fontWeight: FontWeight.w600,
    color: TextColorDark);

const Body1TextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: smallText,
    fontWeight: FontWeight.w700,
    color: TextColorDark);

const Body2TextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: smallText,
    fontWeight: FontWeight.w500,
    color: TextColor2);

const Body3TextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: smallText,
    fontWeight: FontWeight.w400,
    color: TextColor1);

const ButtonTextStyle = TextStyle(
    fontFamily: myFont,
    fontSize: smallText,
    fontWeight: FontWeight.w800,
    color: TextColorDark);
